use url::Url;

#[derive(Clone, Debug)]
pub enum LoginGUI {
    Password(PasswordLoginGUI),
    OAuth(OAuthLoginGUI),
    None,
}

#[derive(Clone, Debug)]
pub struct OAuthLoginGUI {
    pub login_website: Option<Url>,
    pub catch_redirect: Option<String>,
}

#[derive(Clone, Debug)]
pub struct PasswordLoginGUI {
    pub url: bool,
    pub http_auth: bool,
}
