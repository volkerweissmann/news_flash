mod error;
#[cfg(test)]
mod tests;

use self::error::{OpmlError, OpmlErrorKind};
use crate::models::{Category, CategoryID, CategoryType, Feed, FeedID, FeedMapping, Url, NEWSFLASH_TOPLEVEL};
use crate::util::feed_parser::{self, ParsedUrl};
use failure::ResultExt;
use log::warn;
use opml::{Head, Outline, OPML};
use reqwest::Client;
use std::str;

pub struct OpmlResult {
    pub categories: Vec<Category>,
    pub feeds: Vec<Feed>,
    pub feed_mappings: Vec<FeedMapping>,
}

pub fn generate_opml(categories: &[Category], feeds: &[Feed], mappings: &[FeedMapping]) -> Result<String, OpmlError> {
    let mut opml = opml::OPML {
        head: Some(Head {
            title: Some("NewsFlash OPML export".into()),
            ..Head::default()
        }),
        ..Default::default()
    };

    write_categories(categories, feeds, mappings, &NEWSFLASH_TOPLEVEL.clone(), &mut opml.body.outlines);

    let xml_string = opml.to_string().context(OpmlErrorKind::Xml)?;
    Ok(xml_string)
}

pub fn write_categories(categories: &[Category], feeds: &[Feed], mappings: &[FeedMapping], parent_id: &CategoryID, outlines: &mut Vec<Outline>) {
    let filtered_categories: Vec<&Category> = categories.iter().filter(|category| &category.parent_id == parent_id).collect();

    for category in filtered_categories {
        if category.category_type == CategoryType::Generated {
            continue;
        }
        let mut category_outline = Outline {
            title: Some(category.label.clone()),
            text: category.label.clone(),
            ..Outline::default()
        };

        write_categories(categories, feeds, mappings, &category.category_id, &mut category_outline.outlines);
        outlines.push(category_outline);
    }

    let feed_ids: Vec<&FeedID> = mappings
        .iter()
        .filter(|mapping| &mapping.category_id == parent_id)
        .map(|mapping| &mapping.feed_id)
        .collect();

    let feeds: Vec<&Feed> = feeds.iter().filter(|feed| feed_ids.contains(&&feed.feed_id)).collect();

    for feed in feeds {
        if let Some(ref xml_url) = &feed.feed_url {
            outlines.push(Outline {
                text: feed.label.clone(),
                title: Some(feed.label.clone()),
                r#type: Some("rss".into()),
                xml_url: Some(xml_url.to_string()),
                html_url: feed.website.as_ref().map(|url| url.to_string()),
                ..Outline::default()
            });
        }
    }
}

pub async fn parse_opml(opml_string: &str, parse_all_feeds: bool, client: &Client) -> Result<OpmlResult, OpmlError> {
    let opml = OPML::from_str(opml_string).context(OpmlErrorKind::Xml)?;
    let mut category_vec: Vec<Category> = Vec::new();
    let mut feed_vec: Vec<Feed> = Vec::new();
    let mut mapping_vec: Vec<FeedMapping> = Vec::new();
    let mut parsed_feeds: Vec<Feed> = Vec::new();
    let mut sort_index = 0;

    parse_outlines(
        &opml.body.outlines,
        &NEWSFLASH_TOPLEVEL,
        &mut sort_index,
        &mut category_vec,
        &mut feed_vec,
        &mut mapping_vec,
    );

    for feed in feed_vec {
        if parse_all_feeds || feed.website.is_none() {
            let xml_url = match &feed.feed_url {
                Some(url) => url.clone(),
                None => continue,
            };

            match feed_parser::download_and_parse_feed(&xml_url, &feed.feed_id, Some(feed.label.clone()), feed.sort_index, client).await {
                Ok(ParsedUrl::SingleFeed(parsed_feed)) => parsed_feeds.push(parsed_feed),
                Ok(ParsedUrl::MultipleFeeds(_)) => {
                    warn!("Parsing of feed '{}' resulted in multiple available feeds", xml_url);
                    parsed_feeds.push(feed);
                }
                Err(error) => {
                    warn!("Parsing of feed '{}' failed, falling back to data from opml: {}", xml_url, error);
                    parsed_feeds.push(feed);
                }
            }
        } else {
            parsed_feeds.push(feed);
        }
    }

    Ok(OpmlResult {
        categories: category_vec,
        feeds: parsed_feeds,
        feed_mappings: mapping_vec,
    })
}

#[allow(clippy::too_many_arguments)]
fn parse_outlines(
    outlines: &[Outline],
    category_id: &CategoryID,
    sort_index: &mut i32,
    category_vec: &mut Vec<Category>,
    feed_vec: &mut Vec<Feed>,
    mapping_vec: &mut Vec<FeedMapping>,
) {
    for outline in outlines {
        *sort_index += 1;

        if let Some(xml_url) = &outline.xml_url {
            // feed
            let valid_feed_outline = if let Some(outline_type) = &outline.r#type {
                outline_type == "rss" || outline_type == "atom"
            } else {
                true
            };

            if valid_feed_outline {
                let feed_id = FeedID::new(xml_url);

                // prefer optional "title" attribute, fall back to mandatory "text" attribute
                let title = match &outline.title {
                    Some(title) => title.clone(),
                    None => {
                        if outline.text.is_empty() {
                            "No Title".into()
                        } else {
                            outline.text.clone()
                        }
                    }
                };
                let xml_url = Url::parse(xml_url).ok();
                let mapping = if category_id == &*NEWSFLASH_TOPLEVEL {
                    None
                } else {
                    Some(FeedMapping {
                        feed_id: feed_id.clone(),
                        category_id: category_id.clone(),
                    })
                };
                let website = outline.html_url.as_ref().map(|url| Url::parse(url).ok()).flatten();

                let feed = Feed {
                    feed_id,
                    label: title.to_owned(),
                    website,
                    feed_url: xml_url,
                    icon_url: None,
                    sort_index: Some(*sort_index),
                };

                feed_vec.push(feed);
                if let Some(mapping) = mapping {
                    mapping_vec.push(mapping);
                }
            } else {
                log::warn!("invalid feed outline");
            }
        } else {
            // category
            let title = match &outline.title {
                Some(title) => title.clone(),
                None => {
                    if outline.text.is_empty() {
                        "No Title".into()
                    } else {
                        outline.text.clone()
                    }
                }
            };
            let new_category_id = CategoryID::new(&title);
            let category = Category {
                category_id: new_category_id.clone(),
                label: title.to_owned(),
                sort_index: Some(*sort_index),
                parent_id: category_id.clone(),
                category_type: CategoryType::Default,
            };
            category_vec.push(category);

            parse_outlines(&outline.outlines, &new_category_id, sort_index, category_vec, feed_vec, mapping_vec);
        }
    }
}
