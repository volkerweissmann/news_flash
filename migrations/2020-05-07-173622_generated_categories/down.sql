PRAGMA legacy_alter_table=ON;

ALTER TABLE categories RENAME TO _categories_old;

CREATE TABLE categories (
	category_id TEXT PRIMARY KEY NOT NULL,
	label TEXT NOT NULL,
	parent_id TEXT NOT NULL,
	sort_index INTEGER
);

INSERT INTO categories (category_id, label, parent_id, sort_index)
  SELECT category_id, label, parent_id, sort_index
  FROM _categories_old;

DROP TABLE _categories_old;

PRAGMA legacy_alter_table=OFF;