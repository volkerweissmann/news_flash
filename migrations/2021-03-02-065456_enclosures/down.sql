PRAGMA legacy_alter_table=ON;
PRAGMA foreign_keys=OFF;

DROP TABLE enclosures;

CREATE TABLE enclosures (
	article_id TEXT PRIMARY KEY NOT NULL REFERENCES articles(article_id),
	url TEXT NOT NULL,
	enclosure_type INTEGER NOT NULL
);

DROP TRIGGER on_delete_article_trigger;

CREATE TRIGGER on_delete_article_trigger
	AFTER DELETE ON articles
	BEGIN
		DELETE FROM taggings WHERE taggings.article_id=OLD.article_id;
		DELETE FROM enclosures WHERE enclosures.article_id=OLD.article_id;
        DELETE FROM thumbnails WHERE thumbnails.article_id=OLD.article_id;
	END;


PRAGMA foreign_keys=ON;
PRAGMA legacy_alter_table=OFF;